import os
import re
import sys
def menu():
    """
    Función que limpia la pantalla y muestra nuevamente el menu
    """
    os.system('cls')  # NOTA para windows tienes que cambiar clear por cls
    print("Selecciona una opción")
    print("\t1 - que tengas una longitud de 7")
    print("\t2 - expresiones que no finalicen con vocal")
    print("\t3 - palabra que inicie con M y la siguiente sea una vocal")
    print("\t4 - expresiones encerradas entre comillas")
    print("\t5 - ip´s")
    print("\t6 - fechas")
    print("\t7 - telefonos")
    print("\t8 - correos electronicos")
    print("\t9 - Urls")
    print("\t10 - codigo postal")
    print("\t11 - salir")


while True:
    # Mostramos el menu
    menu()

    # solicituamos una opción al usuario
    opcionMenu = input("inserta un numero dependiendo de la opcion")

    if opcionMenu == "1":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("\D[\w]{7,}", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("\D[\w]{7,}", splitValues[i]).group())
                except AttributeError:
                    print(re.search("\D[\w]{7,}", splitValues[i]))
                    archivo.close()



        input("Has pulsado la opción 1...\npulsa una enter para continuar")


    elif opcionMenu == "2":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search(".*([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z])+$", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search(".*([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z])+$",
                                    splitValues[i]).group())
                except AttributeError:
                    print(
                        re.search(".*([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z])+$", splitValues[i]))
                    archivo.close()

        input("Has pulsado la opción 2...\npulsa una enter para continuar")

    elif opcionMenu == "3":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("^[Mm]([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z]).*", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("^[Mm]([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z]).*",
                                    splitValues[i]).group())
                except AttributeError:
                    print(re.search("^[Mm]([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z]).*",
                                    splitValues[i]))
                    archivo.close()

        input("Has pulsado la opción 3...\npulsa una enter para continuar")

    elif opcionMenu == "4":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)

            splitValues = linea.split(" ")

        for i in range(len(splitValues)):
            if (re.search("('.*')|(\".*\")*", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("('.*')|(\".*\")*", splitValues[i]).group())
                except AttributeError:
                    print(re.search("('.*')|(\".*\")*", splitValues[i]))
        archivo.close()

        input("Has pulsado la opción 4..\npulsa una enter para continuar")

    elif opcionMenu == "5":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search(".*(\d+\.\d+\.\d+).*", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search(".*(\d+\.\d+\.\d+).*", splitValues[i]).group())
                except AttributeError:
                    print(re.search(".*(\d+\.\d+\.\d+).*", splitValues[i]))
                    archivo.close()

        input("Has pulsado la opción 5...\npulsa una enter para continuar")

    elif opcionMenu == "6":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search(".*([0-3][0-9].[0-1][0-9].\d).*", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search(".*([0-3][0-9].[0-1][0-9].\d).*", splitValues[i]).group())
                except AttributeError:
                    print(re.search(".*([0-3][0-9].[0-1][0-9].\d).*", splitValues[i]))
                    archivo.close()


        input("Has pulsado la opción 6...\npulsa una enter para continuar")

    elif opcionMenu == "7":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("(^9[0-9]{9})", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("(^9[0-9]{9})", splitValues[i]).group())
                except AttributeError:
                    print(re.search("(^9[0-9]{9})", splitValues[i]))
                    archivo.close()


        input("Has pulsado la opción 7...\npulsa una enter para continuar")

    elif opcionMenu == "8":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("^[a-zA-Z0-9_\-\.~]{2,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("^[a-zA-Z0-9_\-\.~]{2,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$",
                                    splitValues[i]).group())
                except AttributeError:
                    print(re.search("^[a-zA-Z0-9_\-\.~]{2,}@[a-zA-Z0-9_\-\.~]{2,}\.[a-zA-Z]{2,4}$", splitValues[i]))
                    archivo.close()

        input("Has pulsado la opción 8...\npulsa una enter para continuar")

    elif opcionMenu == "9":
        print("")
        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$",
                                    splitValues[i]).group())
                except AttributeError:
                    print(re.search("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$", splitValues[i]))
                    archivo.close()


        input("Has pulsado la opción 9...\npulsa una enter para continuar")

    elif opcionMenu == "10":
        print("")

        archivo = open("archivo1.txt", "r")
        for linea in archivo.readlines():
            print(linea)
        splitValues = linea.split(" ")
        print("Se encontraron las siguientes coincidencias:")
        for i in range(len(splitValues)):
            if (re.search("[0-9]{5}", splitValues[i])):
                if (i == 0):
                    print("Se encontraron las siguientes coincidencias:")
                try:
                    print(re.search("[0-9]{5}", splitValues[i]).group())
                except AttributeError:
                    print(re.search("[0-9]{5}", splitValues[i]))
                    archivo.close()

        input("Has pulsado la opción 10...\npulsa una enter para continuar")

    elif opcionMenu == "11":
        break
    else:
        print("")
        input("No has pulsado ninguna opción correcta...\npulsa una tecla para continuar")